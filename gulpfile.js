/*global require:false*/
/*eslint no-console:0*/

var
  gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  stylus = require('gulp-stylus'),
  concat = require('gulp-concat'),
  proxyMiddleware = require('http-proxy-middleware'),
  browserSync = require('browser-sync').create(),
  browserify = require('browserify'),
  babelify = require('babelify'),
  source = require('vinyl-source-stream'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  eslint = require('gulp-eslint');

gulp.task('lint', function () {
  return gulp.src(['gulpfile.js', 'app/**/*.js', 'app/**/*.jsx'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('lint-nofail', function () {
  return gulp.src(['gulpfile.js', 'app/**/*.js', 'app/**/*.jsx'])
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('build-watch-sources', ['build'], function () {
  browserSync.reload();
});
gulp.task('build-watch-resources', ['css', 'resources'], function () {
  browserSync.reload();
});

gulp.task('serve', ['build', 'resources'], function () {
  browserSync.init({
    server: {
      baseDir: './'
    },
    port: 9000,
    middleware: proxyMiddleware('/api', {target: 'http://localhost:3333'}),
    open: true
  });

  // add browserSync.reload to the tasks array to make
  // all browsers reload after tasks are complete.
  gulp.watch(['app/**/*.js'], ['build-watch-sources']);
  gulp.watch(['app/assets/**/*.styl'], ['build-watch-resources']);
});

gulp.task('serveIntro', function () {

  browserSync.init({
    server: {
      baseDir: './introductionToReactJS'
    },
    port: 9999,
    open: true
  });

  gulp.watch([
    'introductionToReactJS/slides.md',
    'introductionToReactJS/index.html',
    'introductionToReactJS/style.css'
  ], browserSync.reload);

});

gulp.task('css', function () {
  return gulp.src(['app/assets/**/*.css', 'app/assets/**/*.styl'])
    .pipe(plumber())
    .pipe(stylus())
    .on('error', function (err) {
      console.error(err);
    })
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./dist/styles'));
});

gulp.task('resources', function () {

  // copy some 3rd party styles to our dist folder
  gulp.src([
    './node_modules/font-awesome/css/font-awesome.min.css'
  ])
    .pipe(concat('3rdParty.css'))
    .pipe(gulp.dest('./dist/styles'));

  // copy fonts from 'font-awesome' to our dist-folder
  gulp.src('./node_modules/font-awesome/fonts/*')
    .pipe(gulp.dest('./dist/fonts'));

  gulp.src(['app/assets/img/*'])
    .pipe(gulp.dest('./dist/img'));
  gulp.src(['app/assets/favicon.ico'])
    .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['css', 'lint-nofail'], function () {

    browserSync.notify('Building...', 5000);

    return browserify({
      entries: 'app/app.js',
      debug: true
    })
      .transform(babelify.configure({
        ignore: /node_modules/,
        presets: ['es2015', 'react']
      }))
      .bundle()
      .on('error', function (err) {
        console.error(err.message);
        this.emit('end');
      })
      .pipe(plumber())
      .pipe(source('bundle.js'))
      .pipe(gulp.dest('dist'));
  }
);

gulp.task('compress', ['lint', 'build'], function () {

  gulp.src('./index.production.html')
    .pipe(rename('index.html'))
    .pipe(gulp.dest('./dist'));

  gulp.src('./dist/bundle.js')
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['compress']);
