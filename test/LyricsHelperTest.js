import assert from 'assert';
import LyricsHelper from '../app/services/LyricsHelper.js';

describe('LyricsHelper', function () {

  describe('#getLineData()', function () {

    it('singleLine', function () {
      const singleLine = 'Some single line';
      let lineData = LyricsHelper.getLineData(singleLine);
      assert.deepEqual(lineData, [
        {
          end: singleLine.length,
          line: singleLine,
          start: 0,
          chords: []
        }
      ]);
    });

    it('twoLines', function () {
      const twoLines = 'The first line\nAnd the second one';
      let lineData = LyricsHelper.getLineData(twoLines);
      assert.deepEqual(lineData, [
        {
          end: 14,
          line: 'The first line',
          start: 0,
          chords: []
        },
        {
          end: 33,
          line: 'And the second one',
          start: 15,
          chords: []
        }
      ]);
    });

  })

});
