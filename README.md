# ReactJS Playground

This is a small playground project to make the first steps with __React__.

Before attending any of our tutorial sessions, please set up a development environment as described below.

## Setup

Below we describe the basic steps to set up a development environment that gets you ready to start coding and experimenting with _React_.
This should not you take longer than 15 minutes. If you already have _git_ set up, you should be even faster.


### Prerequisites
To run and use the code in this repository, you need the following software on your computer:
 
 - git ([Tutorial for Bitbucket](https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html))
    - Optionally [Configure  _git_ with _ssh_ on Bitbucket](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html)
      (Note that this does not work within the normal Zühlke network - you'll have to use the _Zuehlke Red_ Wifi)
 - node 4.4.7 LTS or higher ([Download from node website](https://nodejs.org/en/download/))
 
Installing node, will also install the node package manager _npm_, which we'll use to download and install the dependencies of this project.

### Checking out the code

Checkout this repository via _git_. On the command line use:

    git clone https://bitbucket.org/hatchteam/reactplayground.git

    # or if you configured git to work over ssh with your bitbucket account
    git clone git@bitbucket.org:hatchteam/reactplayground.git

Which will download the source code to a local folder `reactplayground`.

### Installing Dependencies

The `package.json` file in the root directory of the new `reactplayground` folder, contains a list of dependencies and their respective package versions.
Using the command

    npm install
    
this information will be read and the packages will be downloaded and installed into a local `node_modules` folder.

After following these basic steps, you're ready to go.

### Running the app
You can build and start the server by running

    gulp serve
    
on the command line. A _node_ webserver will be started and the app will be available in your browser on

    http://localhost:9000

If you want to take a look at the code, we recommend you start in the `app` folder with the `app.js` file.
It is the root component of the application and the entry point of the render logic.
    
### Development Environment

We recommend you also set up some tools to simplify development with _React_.

We mostly use [Webstorm](https://www.jetbrains.com/webstorm/) which natively supports _React_ and its _JSX_ syntax.

Alternatively, you can also use a sophisticated text editor like [Sublime Text](https://www.sublimetext.com/) or [Atom](https://atom.io/)
which are best complemented with some powerful plugins for _JavaScript_, _node_ and _React_ development.


## Slides

 - Serve Slides (intro to React) by executing ```gulp serveIntro```
 - Open the slides with your browser on http://localhost:9999
  
## Example and Exercises

Our example app fetches data from a webservice. You'll find the documentation here http://transport.opendata.ch/docs.html
 
## Resources

 - [React Documentation](https://facebook.github.io/react/docs/getting-started.html)