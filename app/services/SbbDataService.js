import aja from 'aja';
import Q from 'q';

const SBB_BASE_URL = 'http://transport.opendata.ch/v1';

// use this if you are offline and want to use local mockData
//const SBB_BASE_URL = '/mockData';

/**
 * an example of a simple service to fetch some data from a backend or webservice.
 * this is just plain JavaScript (no react involved)
 *
 * Docs: http://transport.opendata.ch/docs.html
 */
const SbbService = {

  /**
   * searches for SBB stations that match the given query
   *
   * @param {string} query
   * @returns {Promise}
   */
  searchStations: function (query) {
    return getRequest(SBB_BASE_URL + '/locations?query=' + query)
      .then(response => {
        return response.stations ? response.stations : [];
      });
  },

  /**
   * loads the next 10 departures for a given station
   *
   * @param {string} station
   * @returns {Promise}
     */
  getNextDeparturesFromStation: function (station) {
    return getRequest(SBB_BASE_URL + '/stationboard?limit=10&station=' + station)
      .then(response => {
        return response.stationboard ? response.stationboard : [];
      });
  }

};

/**
 * sends a XHTTP GET request to the given url
 *
 * @param url
 * @returns {Promise}
 */
function getRequest(url) {

  let succeeded = false;
  const deferredResult = Q.defer();

  aja()
    .url(url)
    .cache(false)
    .on('success', data => {
      succeeded = true;
      deferredResult.resolve(data);
    })
    .on('error', err => {
      deferredResult.reject(err);
    })
    .on('end', () => {
      if (!succeeded) {
        deferredResult.reject(new Error('Could not fetch ' + url));
      }
    })
    .go();

  return deferredResult.promise;
}


export default SbbService;
