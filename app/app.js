import log from 'loglevel';
import React from 'react';
import { render } from 'react-dom';

import MainView from './views/MainView';

log.setLevel('debug');

/**
 * This is our top-level React component
 * here, we can do some bootstrapping, stuff like routing etc...
 */
const App = React.createClass({

  render() {

    // for the moment, we always render our main view
    return (
      <MainView />
    );
  }
});

// render the react app and insert it into the DOM tree
render(React.createElement(App), document.getElementById('app-root'));
